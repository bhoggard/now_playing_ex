defmodule NowPlaying do
  @moduledoc """
  Documentation for NowPlaying.
  """

  @doc """
  Hello world.

  ## Examples

      iex> NowPlaying.hello()
      :world

  """
  def hello do
    :world
  end
end
